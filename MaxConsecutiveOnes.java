import java.util.Scanner;

class Solution {

    public int findMaxConsecutiveOnes(int[] nums) {
        int maxCount = 0;
        int currentCount = 0;

        for (int num : nums) {
            if (num == 1) {
                currentCount++;
                maxCount = Math.max(maxCount, currentCount);
            } else {
                currentCount = 0;
            }
        }

        return maxCount;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Solution solution = new Solution();

        System.out.println("Enter the size of the array:");
        int size = scanner.nextInt();

        int[] nums = new int[size];
        System.out.println("Enter the elements of the array (0 or 1):");
        for (int i = 0; i < size; i++) {
            nums[i] = scanner.nextInt();
        }

        int result = solution.findMaxConsecutiveOnes(nums);
        System.out.println("The maximum number of consecutive 1's is: " + result);

    }
}
