import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;

class Solution {

    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        Map<Integer, Integer> nextGreaterMap = new HashMap<>();
        Stack<Integer> stack = new Stack<>();

        for (int num : nums2) {
            while (!stack.isEmpty() && stack.peek() < num) {
                nextGreaterMap.put(stack.pop(), num);
            }
            stack.push(num);
        }

        int[] result = new int[nums1.length];
        for (int i = 0; i < nums1.length; i++) {
            result[i] = nextGreaterMap.getOrDefault(nums1[i], -1);
        }

        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Solution solution = new Solution();

        System.out.println("Enter the size of nums1:");
        int size1 = scanner.nextInt();
        int[] nums1 = new int[size1];
        System.out.println("Enter the elements of nums1:");
        for (int i = 0; i < size1; i++) {
            nums1[i] = scanner.nextInt();
        }

        System.out.println("Enter the size of nums2:");
        int size2 = scanner.nextInt();
        int[] nums2 = new int[size2];
        System.out.println("Enter the elements of nums2:");
        for (int i = 0; i < size2; i++) {
            nums2[i] = scanner.nextInt();
        }

        int[] result = solution.nextGreaterElement(nums1, nums2);

        System.out.println("Output: " + Arrays.toString(result));

    }
}
